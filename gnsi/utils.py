"""Les fonctions communes de l'interface cli."""
from colorama import Fore, Back, Style

def message_erreur(message):
    """Affiche les messages d'erreurs en rouge."""
    print(Back.RED + message + Style.RESET_ALL)

def choisir_parmi_liste(invite, liste):
    """Affiche l'invite puis attends une réponse adéquate de l'utilisateur.

    Si liste = ['Seconde', 'Première'], le menu proposé sera :

    (s)econde

    (p)remiere

    L'utilisateur devra choisir s ou p.
    """
    reponse = "ù"
    dico_choix = {}
    print(invite)
    print("Choisissez parmi les options suivantes :")
    for proposition in liste:
        lettre = proposition[0].lower()
        dico_choix[proposition[0].lower()] = proposition
        print(f"({lettre}){proposition[1:]}")
    while reponse not in dico_choix.keys():
        reponse = input("Votre choix : ")
    return dico_choix[reponse]

    
