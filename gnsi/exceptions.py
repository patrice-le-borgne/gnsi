class GNSIExceptions(Exception):
    pass

class GNSIConfigError(Exception):
    def __init__(self, message=None, **kwargs):
        self.message = "Le dossier de configuration n'existe pas ou est pointe vers de mauvais dossiers. !"

class GNSIBrouillonNotEmptyError(GNSIExceptions):
    def __init__(self, message=None, **kwargs):
        self.message = "Le dossier brouillons n'est pas vide !"

class GNSIBrouillonEmptyError(GNSIExceptions):
    def __init__(self, message=None, **kwargs):
        self.message = "Le dossier brouillons est vide !"

class GNSIBrouillonImportFormatError(GNSIExceptions):
    def __init__(self, message=None, file = None, **kwargs):
        self.message = f"Le format de {file} n'est pas supporté !"
        
class GNSIFileNotFoundError(GNSIExceptions):
    def __init__(self, message="Le fichier n'existe pas", **kwargs):
        self.message = message


class GNSITemplateError(GNSIExceptions):
    def __init__(self, message=None, **kwargs):
        self.message = "Une erreur s'est produite lors de l'utilisation d'un template Jinja2."


class GNSINameFileError(GNSIExceptions):
    def __init__(self, message=None, **kwargs):
        self.message = "Le nom du fichier est incorrect."

class GNSINameDirectoryError(GNSIExceptions):
    def __init__(self, message= "Le dossier du fichier est incorrect.", **kwargs):
        self.message = message


class GNSIArgumentError(GNSIExceptions):
    def __init__(self, message=None, **kwargs):
        self.message = "Le nombre d'arguments est incorrect."

class GNSIDocumentError(GNSIExceptions):
    def __init__(self, message=None, **kwargs):
        self.message = "Le nom du dossier ou du fichier destination n'est pas précisé."

class GNSINotZipFileError(GNSIExceptions):
    def __init__(self, message=None, **kwargs):
        self.message = "Le fichier n'est pas une archive zip."

class GNSIClasseNotExist(GNSIExceptions):
    def __init__(self, classe="", **kwargs):
        self.message = f"La classe {classe} n'existe pas."


