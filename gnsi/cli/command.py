"""Les callbacks du programme principal."""

import shutil
import subprocess
from os import chdir
from pathlib import Path, PurePath
from ..config import get_dossier, get_executable
#from ..config import get_dossier_latex, get_dossier_pdf, get_dossier_templates
from ..exceptions import GNSIBrouillonNotEmptyError, GNSIBrouillonImportFormatError
from ..classes import Brouillon, Devoir, DevoirCorrection, Correcteur
from ..utils import choisir_parmi_liste

#######################################
#####          Brouillon         ######
#######################################

def clean_brouillons():
    """Initialise le dossier brouillons."""
    brouillon = Brouillon()
    brouillon.clean()
    
def import_to_brouillons(*args):
    """
    Copie fichier dans le dossier brouillons.
    """
    brouillon = Brouillon()
    brouillon.importer(*args)
    
def tester_brouillon(force):
    """
    Génére tous les formats à partir du fichier md.
    """
    brouillon = Brouillon()
    brouillon.tester(force)

def visualiser_pdf():
    """
    Compile et visualise le pdf du brouillon
    """
    brouillon = Brouillon()
    brouillon.visualiser_pdf()

def preparer():
    """
    Récupèrer un nom de dossier pour l'exercice.
    """
    brouillon = Brouillon()
    brouillon.preparer()

def exporter():
    brouillon = Brouillon()
    brouillon.exporter()

def corriger_exercice(*args):
    brouillon = Brouillon()
    brouillon.importer_pour_correction(*args)

#######################################
#####           Devoir           ######
#######################################

def creer_devoir(dossier, titre, auteur, date, exercices):
    devoir = Devoir(dossier, titre, auteur, date, exercices)
    devoir.creer_fichier_latex()
    devoir.visualiser_pdf()
    devoir.importer_fichiers_annexes()
    
def corriger_devoir(dossier):
    devoir = DevoirCorrection(dossier)
    devoir.importer_fichiers_annexes()

def ramasser(dossier, archive, classe):
    devoir = DevoirCorrection(dossier)
    devoir.ramasser(archive, classe)

#######################################
#####         Correction         ######
#######################################
    
def corriger(devoir):
    correcteur = Correcteur(devoir)
    correcteur.corriger()
    correcteur.exporter_resultats_markdown()
    correcteur.exporter_fichier_bilan()
