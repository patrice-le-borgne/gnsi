"""Le programme de gestion des évaluations de la NSI en ligne de commandes."""
__version__ = "0.1"

from .__main__ import cli_start
