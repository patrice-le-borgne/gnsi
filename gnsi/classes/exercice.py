""" Le module qui permet de gérer préparer les exercices
avant le tranfert vers le devoir. 
"""

import shutil
import subprocess
from os import chdir
from pathlib import Path, PurePath
from ..config import get_dossier, get_executable
from ..exceptions import GNSINameDirectoryError, GNSIFileNotFoundError
#import pandoc
#from pandoc.types import *
import re
import csv

class Exercice:
    def __init__(self, nom, n):
        "La classe qui gère un exercice."
        self.dossier_exercice = Path(get_dossier("exercices")) /nom
        if not self.dossier_exercice.exists():
            raise GNSINameDirectoryError(f"{nom} n'existe pas.")
        self.fichier_python = self.dossier_exercice/"exercice.py"
        self.fichier_bareme = self.dossier_exercice/"bareme.csv"
        self.fichier_bareme_amc = self.dossier_exercice/"bareme_amc.csv"
        self.dossier_latex = self.dossier_exercice/"latex"
        self.fichier_latex = self.dossier_latex / "exercice.tex"
        self.dossier_docs = self.dossier_exercice/"docs"
        self.numero = n

    def get_latex(self):
        """
        Renvoie l'énonce latex en changeant le numéro de l'exercice.
        """
        n = self.numero
        text = self.fichier_latex.read_text()
        apres = "section{Exercice "+ str(n) + "}"
        pattern = re.compile(r"section\{(.*?)\}")
        text1 = re.sub(pattern, apres, text)
        pattern = re.compile(r"label\{exercice(.*?)\}")
        apres = "label{exercice"+ str(n) + "}"
        text2 = re.sub(pattern, apres, text1)
        return text2

    def envoyer_images(self, dossier):
        """
        Envoyer les images du dossier latex dans dossier.
        """
        extensions = ["*.png", "*.jpg", "*.jpeg"]
        liste_images = []
        for ext in extensions:
            liste_images += self.dossier_latex.glob(ext)
        liste_images = set(liste_images)
        for fichier in liste_images:
            nom = PurePath(fichier).name
            dst = dossier / nom
            shutil.copyfile(str(fichier), str(dst))

    def envoyer_tests(self, dossier):
        """
        Envoie les fichiers tests dans le dossier.
        """
        prefixe = "exercice_{}_".format(self.numero)
        for fichier in self.dossier_docs.iterdir():
            text = self.contenu_test(fichier)
            nom = prefixe + PurePath(fichier).name
            dst = dossier / nom
            dst.write_text(text)

    def envoyer_bareme(self, dossier):
        """
        Ajouter le bareme de l'exercice à celui qui se trouve dans dossier.
        """
        text = self.fichier_bareme.read_text()
        lignes = text.split("\n")
        bareme_modifie = []
        for ligne in lignes:
            if len(ligne)>1:
                bareme_modifie.append("Exercice {},exercice_{}_{}".format(self.numero,self.numero,ligne))
        fichier_bareme = dossier / "bareme.csv"
        with open(fichier_bareme, "a") as dst:
            dst.write("\n".join(bareme_modifie)+"\n")

    def envoyer_fichiers_eleves(self, dossier):
        """
        Ajoute les fichiers élèves dans dossier.
        """
        fichier_eleve = self.dossier_exercice / "exercice_version_eleve.py"
        nom = "exercice_{}.py".format(self.numero)
        dst = dossier / nom
        if fichier_eleve.exists():
            shutil.copyfile(str(fichier_eleve), str(dst))
        fichier_eleve = self.dossier_exercice / "exercice_version_eleve.md"
        nom = "exercice_{}.md".format(self.numero)
        dst = dossier / nom
        if fichier_eleve.exists():
            shutil.copyfile(str(fichier_eleve), str(dst))
            
    def get_valeurs_amc(self):
        """ Renvoie une liste de dictionnaire pour remplir les templates AMC."""
        liste = []
        with open(self.fichier_bareme_amc, 'r') as csvfile:
            reader = csv.reader(csvfile)
            for ligne in reader:
                dico = {}
                identifiant = f"exo{self.numero}-{ligne[0]}"
                dico["id"] = identifiant
                question = ligne[0]
                dico["nquestion"] = question
                point = ligne[1].strip(" ")
                liste.append([dico,point])
            return liste
                
    def contenu_test(self, fichier):
        """
        Renvoie le contenu du fichier de test
        """
        n = self.numero
        fichier_test = self.dossier_docs / fichier
        if not fichier_test.exists():
            raise GNSIFileNotFoundError(f"{fichier_test} n'existe pas.")
        text = fichier_test.read_text()
        apres = f"from exercice_{n} import"
        pattern = re.compile(r"from (.*?)exercice (.*?)import")        
        text1 = re.sub(pattern, apres, text)
        return text1
