""" Le module qui permet de gérer le dossier exercices. """

import shutil
import subprocess
from os import chdir
from pathlib import Path, PurePath
from ..config import get_dossier, get_executable, get_nom_exercice
from ..exceptions import GNSIBrouillonNotEmptyError, GNSIBrouillonImportFormatError, GNSIFileNotFoundError
import pandoc
from pandoc.types import *
import re


class GestionnaireExercice:
    def __init__(self):
        "La classe qui gère un exercice."
        self.dossier_exercices = Path(get_dossier("exercices"))

    def nombre_exercices(self):
        """
        Renvoie le nombre d'exercices.
        """
        compteur = 0
        liste = []
        base = get_nom_exercice()
        pattern = base + r"\d\d\d"
        for f in self.dossier_exercices.iterdir():
            a = re.search(pattern ,str(f))
            if a is not None:
               compteur += 1
        return compteur
    
    def generer_nom_exercices(self):
        n = self.nombre_exercices() + 1
        base = get_nom_exercice()+"{:0>3}" 
        return base.format(n)

    def creer_dossier(self, nom):
        """
        Créer un dossier pour recevoir un nouvel exercice.
        """
        racine = self.dossier_exercices / nom
        if not racine.exists():
            racine.mkdir()
        return racine
    
