""" Le module qui permet de tester le fichier markdown"""

import shutil
import subprocess
import re
import pandoc
from pandoc.types import *
from os import chdir
from pathlib import Path, PurePath
from ..config import get_dossier, get_executable
from ..exceptions import GNSIBrouillonNotEmptyError, GNSIBrouillonImportFormatError, GNSIFileNotFoundError, GNSINameDirectoryError
from .gestionnaire_exercice import *


class Brouillon:
    def __init__(self):
        "La classe qui gère le brouillon."
        self.dossier_brouillons = Path(get_dossier("brouillons"))
        self.fichier_source = self.dossier_brouillons/"exercice.md"
        self.fichier_source_version_eleve = self.dossier_brouillons/"exercice_version_eleve.md"
        self.fichier_python = self.dossier_brouillons/"exercice.py"
        self.fichier_bareme = self.dossier_brouillons/"bareme.csv"
        self.fichier_bareme_amc = self.dossier_brouillons/"bareme_amc.csv"
        self.dossier_latex = self.dossier_brouillons/"latex"
        self.fichier_source_latex = self.dossier_latex/"exercice.tex"
        self.dossier_docs = self.dossier_brouillons/"docs"

    def clean(self):
        """
        Nettoie le dossier brouillons
        """

         # Dans un premier temps, on vide tous les dossiers.
        dossier_brouillons = self.dossier_brouillons
        for f in dossier_brouillons.iterdir():
            if not f.is_dir():
                f.unlink()
            else:
                for g in f.iterdir():
                    g.unlink()

        # on recopie le fichier brouillon.tex du dossier modeles au dossier brouillons/latex
        dossier_modeles = Path(get_dossier("modeles"))
        dossier_latex = self.dossier_latex
        src = dossier_modeles / "brouillon.tex"
        dst = dossier_latex / "brouillon.tex"
        shutil.copyfile(str(src), str(dst))
        src = dossier_modeles / "setup.cfg"
        dst = dossier_brouillons / "setup.cfg"
        shutil.copyfile(str(src), str(dst))


    def est_vide(self):
        """ On vérifie d'abord que le dossier brouillon est vide. """
        count = 0
        for f in self.dossier_brouillons.iterdir():
            if not f.is_dir():
                count += 1
        if count > 1:
            raise GNSIBrouillonNotEmptyError()

    def importer(self, *args):
        """
        Import les fichiers donnés en arguments en les plaçant au bon endroit en fonction de leur extension.
        """
        self.est_vide()
        for fichier in args[0]:
            # On vérifie que le fichier existe
            src = Path(fichier)
            if not src.exists():
                raise GNSIFileNotFoundError(f"Le fichier{fichier} n'existe pas.")
            # on l'importe
            suffixe = PurePath(fichier).suffix 
            if  suffixe == ".md":
                shutil.copyfile(fichier, str(self.fichier_source))
            elif suffixe in (".png",".jpeg", ".jpg"):
                nom_fichier = PurePath(fichier).name
                dst = self.dossier_latex / nom_fichier
                shutil.copyfile(fichier, str(dst))
            elif suffixe == ".py":
                shutil.copyfile(fichier, str(self.fichier_python))
            else:
                raise GNSIBrouillonImportFormatError(file = fichier)



        
    def version_eleve(self, frmt = "latex"):
        """
        Renvoie l'énoncé sans les block cachés.
        frmt : format de sortie. Doit être compatible avec pandoc.
        """
        source = pandoc.read(file=self.fichier_source, format = "markdown")
        version_eleve = pandoc.read("")
        for element in source[1]:
            if isinstance(element, CodeBlock):
                if 'hidden' not in element[0][1] :
                    element[0] = ('', ['python'], [])
                    version_eleve[1].append(element)
            else:
                version_eleve[1].append(element)
        return pandoc.write(version_eleve, format = frmt)
    
    def generer_latex(self):
        """
        Génère le code latex à partir du fichier markdown initial.
        """
        dst = self.fichier_source_latex
        dst.write_text(self.version_eleve())

    def generer_version_eleve(self):
        """
        Génère le code latex à partir du fichier markdown initial.
        """
        dst = self.fichier_source_version_eleve
        dst.write_text(self.version_eleve("markdown"))


    def visualiser_pdf(self):
        """
        Lance la compilation et la visualisation du brouillon
        """
        chdir(self.dossier_latex)
        latex = get_executable("latex")
        reader = get_executable("reader")
        subprocess.call([latex, 'brouillon.tex'])
        subprocess.call([reader, 'brouillon.pdf'])
    

    def tester(self, force):
        """
        Génère le code latex à partir du fichier markdown initial.
        """
        # On ne génère le source latex qu'une seule fois.
        dst = self.fichier_source_latex
        if not dst.exists() or force:
            self.generer_latex()
            self.generer_version_eleve()
        # On extrait les tests du fichier source puis on lance les tests.
        self.generer_tests()
        self.tests_concrets()
        # Prépare les fichiers baremes
        print("Bareme")
        bareme = self.bareme()
        print(bareme)
        print("Bareme AMC")
        bareme_amc = self.bareme_amc()
        print(bareme_amc)
        self.fichier_bareme.write_text(bareme)
        self.fichier_bareme_amc.write_text(bareme_amc)
        
    def _get_nom_fichier(self, l):
        """
        Cherche le nom du fichier parmi des tuples.
        """
        for cle , valeur in l:
            if cle == 'file':
                return valeur
        raise ValueError

    def _get_bareme(self, l):
        """
        Cherche le nom du fichier parmi des tuples.
        """
        for cle , valeur in l:
            if cle == 'bareme':
                return valeur
        raise ValueError

    def _get_entete(self):
        source = pandoc.read(file=self.fichier_source, format = "markdown")
        entete = pandoc.read("")
        for element in pandoc.iter(source):
            if isinstance(element, CodeBlock):
                if 'all' in element[0][1] :
                    element[0] = ('', [], [])
                    entete[1].append(element)
        return entete 
        
    def generer_tests(self):
        """
        Génère les fichiers tests à partir du fichier source.
        """
        source = pandoc.read(file=self.fichier_source, format = "markdown")
        for element in pandoc.iter(source):
            if isinstance(element, CodeBlock):
                if 'test' in element[0][1] :
                    nom = self.dossier_docs / self._get_nom_fichier(element[0][2])
                    element[0] = ('', [], [])
                    test = self._get_entete()
                    test[1].append(element)
                    test[1].append(Para([Str(" ")]))
                    pandoc.write(test, file=str(nom), format = "markdown")
   
    def tests_concrets(self):
        """
        Lance les tests sur le fichier.
        """
        chdir(self.dossier_brouillons)
        test = get_executable("test")
        subprocess.call([test])
        
    def bareme(self):
        """
        Renvoie le bareme amc.
        """
        source = pandoc.read(file=self.fichier_source, format = "markdown")
        contenu = ""
        for element in pandoc.iter(source):
            if isinstance(element, CodeBlock):
                if 'test' in element[0][1] :
                    nom = self._get_nom_fichier(element[0][2])
                    points = self._get_bareme(element[0][2])
                    contenu += nom + ", " + points + "\n"
        return contenu


    def bareme_amc(self):
        """
        Renvoie le bareme amc.
        """
        source = pandoc.read(file=self.fichier_source, format = "markdown")
        contenu = ""
        for element in pandoc.iter(source):
            if isinstance(element, CodeBlock):
                if 'amc' in element[0][1] :
                    nom = self._get_nom_fichier(element[0][2])[:-3]
                    nom = nom.replace("_",".")
                    points = self._get_bareme(element[0][2])
                    contenu += nom + ", " + points + "\n"
        return contenu


    def numeroter_latex(self, n = 1):
        """
        Rajouter un numero à l'exercice.
        """
        text = self.fichier_source_latex.read_text()
        apres = "section{Exercice "+ str(n) + "}"
        pattern = re.compile(r"section\{(.*?)\}")
        text1 = re.sub(pattern, apres, text)
        pattern = re.compile(r"label\{exercice(.*?)\}")
        apres = "label{exercice"+ str(n) + "}"
        text2 = re.sub(pattern, apres, text1)
        self.fichier_source_latex.write_text(text2)

    def changer_titre(self, titre = "Brouillon"):
        """
        Change le titre du brouillon
        """
        document = self.dossier_latex / "brouillon.tex"
        text = document.read_text()
        apres = "title{" + titre + "}"
        pattern = re.compile(r"title\{(.*?)\}")
        text1 = re.sub(pattern, apres, text)
        document.write_text(text1)


    def preparer(self):
        """
        Prépare l'exportation
        """
        src = self.dossier_brouillons
        correction = src / "correction"
        if correction.exists():
            nom = correction.read_text()
        else:
            gestexo = GestionnaireExercice()
            nom = gestexo.generer_nom_exercices()
        self.changer_titre(nom)
        self.visualiser_pdf()
        src = self.dossier_latex / "brouillon.pdf"
        nom_fichier = nom + ".pdf"
        dst = self.dossier_latex / nom_fichier
        shutil.copyfile(str(src), str(dst))

    def exporter(self):
        """
        Exporte le brouillon vers le dossier exercice.
        """
        src = self.dossier_brouillons
        correction = src / "correction"
        if correction.exists():
            nom = correction.read_text()
            dst =  Path(get_dossier("exercices")) / nom
            shutil.rmtree(dst)
        else:
            gestexo = GestionnaireExercice()
            nom = gestexo.generer_nom_exercices()
        dst =  Path(get_dossier("exercices")) / nom
        shutil.copytree(str(src), str(dst), ignore = shutil.ignore_patterns('brouillon.*', 'correction'))


    def importer_pour_correction(self, nom):
        """
        Transfert un exercice existant dans les brouillons.
        """
        exercice = Path(get_dossier("exercices")) / nom
        if not exercice.exists():
            raise GNSINameDirectoryError(f"{exercice} n'existe pas !")
        self.est_vide()
        shutil.rmtree(self.dossier_brouillons)
        shutil.copytree(str(exercice), str(self.dossier_brouillons))
        dossier_modeles = Path(get_dossier("modeles"))
        dossier_latex = self.dossier_latex
        src = dossier_modeles / "brouillon.tex"
        dst = dossier_latex / "brouillon.tex"
        shutil.copyfile(str(src), str(dst))
        correction = self.dossier_brouillons / "correction"
        correction.write_text(nom)
        self.changer_titre(nom)
