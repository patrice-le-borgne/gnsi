"""Les classes qui générent le code latex des documents à partir de templates.

Les templates seront recherchés dans le dossier Moèles défini dans le fichier de configuration.
"""
from pathlib import Path
import logging
import jinja2
from ..config import get_dossier
from ..exceptions import *


document_jinja_env = jinja2.Environment(
    block_start_string='\BLOCK{',
    block_end_string='}',
    variable_start_string='\VAR{',
    variable_end_string='}',
    comment_start_string='\#{',
    comment_end_string='}',
    line_statement_prefix='%%',
    line_comment_prefix='%#',
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader(str(get_dossier("modeles")))
)

class jinjaBase:
    """Classe de base avec les méthodes communes aux documents et exercices"""
    def __init__(self, env = document_jinja_env):
        self.env= env

    def get_latex(self, template_fichier=None, valeurs={}):
        """ Charge le fichier template puis le complète avec le dictionnaire valeurs."""
        if template_fichier is None:
            raise(GNSITemplateError(f"Il manque le nom du template à utiliser."))
        try:
            template = self.env.get_template(template_fichier)
        except:
            raise(GNSITemplateError(f"Problème lors du chargement du template {template_fichier} !"))
 
        try:
            if valeurs == {}:
                  contenu= template.render()
            else:
                  contenu= template.render(valeurs)
        except:
            raise(GNSITemplateError(f"Problème lors de l'utilisation du template {template_fichier} !"))
        return contenu
